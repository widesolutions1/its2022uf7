package it.sinergy.idc.istagramm;

public class IstagramGetterException extends Exception {
  public IstagramGetterException(String message, Throwable cause) {
    super(message, cause);
  }
}
