package it.sinergy.idc.istagramm;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.time.Instant;

public class IstagramGetter {

  private static final String ISTAGRAM_URL="https://www.instagram.com/graphql/query/?query_hash=f2405b236d85e8296cf30347c9f08c2a&variables=";
  private static final Logger logger = LogManager.getLogger(IstagramGetter.class);

  private final long minTimeBetweenCalls;
  private  long lastCallTime;
  private final ObjectMapper objectMapper = new ObjectMapper();
  private static Proxy proxy ;

  private static IstagramGetter instance;

  public static IstagramGetter getInstance(){
    return getInstance(10000);
  }

  public static IstagramGetter getInstance(long minTimeBetweenCalls){
    if (null == instance){
      synchronized (IstagramGetter.class) {
        instance = new IstagramGetter(minTimeBetweenCalls);
        proxy =  createProxy();
      }
    }

    return instance;
  }

  private static Proxy createProxy(){
    System.setProperty("http.auth.preference","basic");
    System.setProperty("jdk.http.auth.tunneling.disabledSchemes","");
    Authenticator.setDefault(
            new Authenticator() {
              @Override
              public PasswordAuthentication getPasswordAuthentication() {

                return new PasswordAuthentication("imagednl", "Dmr163321".toCharArray());
              }
            }
    );
    return   new Proxy(Proxy.Type.HTTP, new InetSocketAddress("be.smartproxy.com",40000));
  }

  private IstagramGetter(long minTimeBetweenCalls){
    this.minTimeBetweenCalls = minTimeBetweenCalls;
    updateLastCallTime();
  }

  public JsonNode getNextPage(String id) throws IstagramGetterException {
    return getNextPage(id,30,"");
  }

  public JsonNode getNextPage(String id, Integer first, String after) throws IstagramGetterException {

    JsonNode retVal;
    long diff = Instant.now().toEpochMilli() - lastCallTime;
    if (diff < minTimeBetweenCalls) {
      try {
        logger.info("Waiting for {} ms to avoid to moore request and finish in black list :-) ", minTimeBetweenCalls- diff);
        Thread.sleep(minTimeBetweenCalls- diff);
      } catch (InterruptedException e) {
        throw new IstagramGetterException("Waiting thread interrupted", e);
      }
    }

    logger.info("Starting Istagram request...");
    URLConnection connection;
    try {
      URL url = new URL(composeUrl(id,first,after));
      updateLastCallTime();
      connection = url.openConnection(proxy);

    } catch (IOException e) {
      throw new IstagramGetterException("Error connecting to istagram", e);
    }
    try (InputStream is = connection.getInputStream()) {
      updateLastCallTime();
      retVal = objectMapper.readTree(is);
    } catch (IOException e) {
      throw new IstagramGetterException("Error reading data from Istagram", e);
    }
    logger.info("Istagram request done");
    return retVal;
  }

  private void updateLastCallTime(){
    lastCallTime = Instant.now().toEpochMilli();
  }

  private String composeUrl(String id, Integer first, String after) throws IstagramGetterException {

    String retVal;
    RequestBody.builder().first(30).id(id).after(after).build();

    ByteArrayOutputStream bos = new ByteArrayOutputStream();

    try {
      URLEncoder.encode(bos.toString(),"UTF-8");
      objectMapper.writeValue(bos, RequestBody.builder().first(30).id(id).after(after).build());
      retVal = ISTAGRAM_URL+URLEncoder.encode(bos.toString(),"UTF-8");
    } catch (IOException  e) {
      throw new IstagramGetterException("Error creating URL address", e);
    }
    return retVal;
  }

  @Getter
  @Setter
  @Builder
  private static class RequestBody {
    String id;
    Integer first;
    String after;
  }
}
