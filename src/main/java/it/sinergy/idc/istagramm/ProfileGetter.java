package it.sinergy.idc.istagramm;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

public class ProfileGetter {

  public void downloadProfile(String id, OutputStream os) throws IstagramGetterException {
   JsonNode n, edgeOwnerToTimelineMedia, edges;
   String after = "";

   JsonFactory jsonFactory = new JsonFactory();
   jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
   ObjectMapper objectMapper = new ObjectMapper(jsonFactory);

   boolean asNext;
    do {

      n = IstagramGetter.getInstance().getNextPage(id,50,after);
      // check response status
      boolean ok = "ok".equals(n.get("status").asText());

      // parse json response
      edgeOwnerToTimelineMedia = n.get("data").get("user").get("edge_owner_to_timeline_media");
      asNext = edgeOwnerToTimelineMedia.get("page_info").get("has_next_page").asBoolean();
      after = edgeOwnerToTimelineMedia.get("page_info").get("end_cursor").asText();

      edges = edgeOwnerToTimelineMedia.get("edges");
      try {
        objectMapper.writeValue(os, edges);
      } catch (IOException e) {
        throw new IstagramGetterException("Error writing file ", e);
      }
    }  while (asNext);
  }


  public void downloadProfile(List<String> ids, OutputStream os) throws IstagramGetterException {
    for (String id : ids){
      downloadProfile(id, os);
    }
  }


}
