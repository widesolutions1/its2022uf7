package it.sinergy.idc;

import it.sinergy.idc.istagramm.IstagramGetterException;
import it.sinergy.idc.istagramm.ProfileGetter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.List;

public class UsageExample {

  public static void main(String[] args) throws IstagramGetterException, FileNotFoundException {
    List<String> ids = Arrays.asList("2159561052","2159561053");
    ProfileGetter pg = new ProfileGetter();
    pg.downloadProfile(ids, new FileOutputStream("C:\\Users\\Francesco\\istagram.json"));
  }
}
